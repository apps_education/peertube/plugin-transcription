import { TranscriptionRequestProcessor } from './TranscriptionRequestProcessor';
import { MessagePort, TransferListItem } from 'worker_threads';
import { SubtitleGenerator } from '../Subtitle/SubtitleGenerator';
import { resolve } from 'path';
import { LanguageModelManager, ModelFactory, RecognizerFactory } from '../Recognizer';
import { EventEmitter } from 'events';
import { jest } from '@jest/globals';
import { languageModelRepository } from '../Repository/__mocks__/LanguageModelRepository';
import { small } from '../__mocks__/models';
import { SettingsRegistry } from '../PeerTube';
import { pluginSettingsManager, registerPluginSetting } from '../__mocks__/PeerTube/pluginSettings';
import { pluginStorageManager } from '../__mocks__/PeerTube/PluginStorageManager';

jest.setTimeout(29000);

class MockedMessagePort extends EventEmitter implements MessagePort {
  close(): void {}
  postMessage(value: any, transferList?: ReadonlyArray<TransferListItem>): void {
    this.emit('message', value);
  }
  ref(): void {}
  start(): void {}
  unref(): void {}
}

describe('TranscriptionRequestProcessor', () => {
  test('TranscriptionRequestProcessor', async () => {
    const logger = console;
    const dataPath = 'data';
    const modelPath = resolve(dataPath, 'models');
    const captionsPath = resolve(dataPath, 'captions');
    const videosPath = resolve(dataPath, 'videos');

    const settingsRegistry = new SettingsRegistry(
      pluginSettingsManager,
      registerPluginSetting,
      pluginStorageManager,
      console
    );
    const languageModelManager = new LanguageModelManager(
      settingsRegistry,
      languageModelRepository,
      console,
      'data/models',
      {
        en: 'English',
      }
    );
    await languageModelManager.downloadAndExtractVoskModel(small);

    const recognizerFactory = new RecognizerFactory(new ModelFactory(modelPath));
    const subtitleGenerator = new SubtitleGenerator(recognizerFactory, logger);
    const messagePort = new MockedMessagePort();

    const transcriptionRequestProcessor = new TranscriptionRequestProcessor(
      messagePort,
      subtitleGenerator,
      captionsPath,
      console
    );

    messagePort.on('message', (result) => {
      expect(result).toEqual(resolve(dataPath, 'captions', 'uuid-en.vtt'));
    });

    await transcriptionRequestProcessor.process({
      uuid: 'uuid',
      language: 'en',
      filePath: resolve(videosPath, 'eye.mp4'),
    });
  });
});
