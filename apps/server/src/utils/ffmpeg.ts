import { ChildProcess, spawn } from 'child_process';
import fs from 'fs';
import { LoggerInterface } from '../Model';

export interface TranscodingProcessOptions {
  inputFilePath: string;
  outputFilePath?: string;
  sampleRate?: number;
  bufferSize?: number;
  logger?: LoggerInterface;
}

export const createTranscodingProcess = ({
  inputFilePath,
  outputFilePath = '',
  sampleRate = 16000,
  bufferSize = 1000,
  logger = console,
}: TranscodingProcessOptions) => {
  if (!fs.existsSync(inputFilePath)) {
    throw new Error(`[transcoding] Input file ${inputFilePath} does not exist`);
  }

  const options = [
    '-i',
    inputFilePath,
    '-vn', // no video
    '-ar',
    sampleRate, // set the audio sampling frequency
    '-ac',
    '1', // set the number of audio channels to 1 since Vosk is expecting mono
    '-f',
    's16le', // PCM signed 16-bit little-endian
    '-bufsize',
    bufferSize, // set a buffer size to provide a steady flow of frames
    '-',
    // outputFilePath, // @todo
    // './data/videos/HowtheEy1941.wav',
  ].map(String);

  const ffmpegProcess = createProcess(options, logger);
  logger.info(`ffmpeg process ${ffmpegProcess.pid} created`);

  return ffmpegProcess;
};

export const createProcess = (options: string[] = [], logger: LoggerInterface = console): ChildProcess => {
  logger.debug(`ffmpeg ${options.join(' ')}`);
  const ffmpegProcess = spawn('ffmpeg', options);

  return ffmpegProcess;
};

export const stopProcess = (process?: ChildProcess, logger: LoggerInterface = console): void => {
  if (process) {
    logger.info(`Attempt to stop process ${process.pid}`);
    if (!process.killed) {
      const killed = process.kill('SIGINT');
      if (killed) {
        logger.info(`ffmpeg process ${process.pid} killed`);
      } else {
        logger.error(`Couldn't kill ffmpeg process ${process.pid}`);
      }

      if (process.stdin) {
        process.stdin.end();
      }
    } else {
      logger.info(`Process ${process.pid} already killed`);
    }
  }
};
