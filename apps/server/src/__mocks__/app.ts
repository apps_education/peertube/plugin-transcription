import express from 'express';

const app = express();

app.get('/api/v1/config/about', (req, res) =>
  res.json({
    instance: { languages: [] },
  })
);

export { app };
