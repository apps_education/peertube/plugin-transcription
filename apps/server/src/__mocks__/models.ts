import { VoskModelDescriptor } from '../Model/VoskModelDescriptor';

export const small: VoskModelDescriptor = {
  lang: 'en-us',
  lang_text: 'US English',
  md5: '5eb44943eb590cf1c7cec5024b586699',
  name: 'vosk-model-small-en-us-0.3',
  obsolete: true,
  size: 37279669,
  size_text: '35.6MiB',
  type: 'small',
  url: 'https://alphacephei.com/vosk/models/vosk-model-small-en-us-0.3.zip',
  version: '0.3',
};

export const smallFr: VoskModelDescriptor = {
  lang: 'fr',
  lang_text: 'French',
  md5: '84a36a0231dac3a9bba6304158f4b39a',
  name: 'vosk-model-small-fr-pguyot-0.3',
  obsolete: true,
  size: 45999104,
  size_text: '43.9MiB',
  type: 'small',
  url: 'https://alphacephei.com/vosk/models/vosk-model-small-fr-pguyot-0.3.zip',
  version: 'pguyot-0.3',
};

export const bigFr: VoskModelDescriptor = {
  lang: 'fr',
  lang_text: 'French',
  md5: 'c1dd60d445cc47c7522264f265c0db2e',
  name: 'vosk-model-fr-0.6-linto-2.2.0',
  obsolete: false,
  size: 1583231041,
  size_text: '1.5GiB',
  type: 'big',
  url: 'https://alphacephei.com/vosk/models/vosk-model-fr-0.6-linto-2.2.0.zip',
  version: '0.6-linto-2.2.0',
};

export const availableModels = [smallFr, bigFr];
