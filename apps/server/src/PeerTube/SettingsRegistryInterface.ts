import { Setting } from './SettingsRegistry';

export interface SettingsRegistryInterface {
  register: (setting: Setting) => Promise<void>;
  set: (key: string, value: string) => void;
  get: (key: string) => Promise<string | undefined>;
  getPrevious: (key: string) => Promise<string | undefined>;
}
