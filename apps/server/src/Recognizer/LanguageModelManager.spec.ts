import { LanguageModelManager } from './LanguageModelManager';
import { languageModelRepository } from '../Repository/__mocks__/LanguageModelRepository';
import { settingsRegistry } from '../PeerTube/__mocks__/SettingsRegistry';
import { smallFr } from '../__mocks__/models';
import { pathExists } from 'fs-extra';
import { jest } from '@jest/globals';

jest.setTimeout(20000);

describe('ModelConfigurator', () => {
  test('downloadAndExtract', async () => {
    const languageModelManager = new LanguageModelManager(
      settingsRegistry,
      languageModelRepository,
      console,
      'data/models',
      { fr: 'Français' }
    );
    await languageModelManager.downloadAndExtractVoskModel(smallFr);
    const exists = await pathExists(`data/models/fr`);
    expect(exists).toBe(true);

    const settingKey = LanguageModelManager.getSettingKey('fr');
    expect(settingKey).toEqual('language_fr');

    const language = LanguageModelManager.getLanguageFromSettingKey(settingKey);
    expect(language).toEqual('fr');
  });
});
