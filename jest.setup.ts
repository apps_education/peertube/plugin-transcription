import { app } from './apps/server/src/__mocks__/app';

export default () =>
  new Promise((resolve, reject) => {
    const port = process.env.PORT || 9001;
    global.server = app
      .listen(port, () => {
        console.log(`\nTest server listening on port ${port}.`);
        resolve(null);
        // done(null);
      })
      .on('error', (e) => {
        // done(e);
        reject(e);
      });
  });
