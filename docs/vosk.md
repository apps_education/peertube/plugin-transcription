# Vosk

## Custom forks

We had to fork these libs to make **Vosk** work for our use case :

> @todo document the reasons

- https://github.com/larriereguichet/vosk-api
- https://github.com/larriereguichet/alphacep-ref-napi
- https://github.com/larriereguichet/alphacep-node-ffi-napi

## Accepted audio format

**Vosk** `Recognizer` accept audio data in PCM 16-bit mono format:

> PCM: Pulse-code modulation, the standard form of digital audio in computers
> 16-bit
> mono

> https://superuser.com/questions/675342/convert-mp3-to-wav-using-ffmpeg-for-vbr/675647

## Update Typescript definition file

```
yarn generate-vosk-types
```

## `vosk-api`

> VSOK Api is just a binary.
> https://github.com/alphacep/vosk-api

`ffmpeg` must be used to convert the video audio to the format handled by vosk:

> When using your own audio file make sure it has the correct format - PCM 16khz 16bit mono

`vosk-api` can be used with node to generate `srt` from an audio file:
https://github.com/alphacep/vosk-api/blob/7ccf743bb65810c67ff02d8d0958c0371c60ae68/nodejs/demo/test_srt.js
Requires ffmpeg to be installed -> probably already installed for Peertube.

Vosk `nodejs` demo uses `node-ffi` under the hood:

> "_node-ffi-napi is a Node.js addon for loading and calling dynamic libraries using pure JavaScript. It can be used to create bindings to native libraries without writing any C++ code._"
> https://github.com/node-ffi-napi

~~This approach has one drawback though, the `vosk` binary must be present on the system thus must be installed in order to be used in a `nodejs` project.~~
*__EDIT :__ There's no need to install the binary locally, a __native module__ is locally built unless a prebuild version is available for your arch.*  
Another approach (as done for `DeepSpeech`) would be to package the `vosk` binary with [node-pre-gyp](https://github.com/mapbox/node-pre-gyp).
The `vosk` binary could be built, packaged and deployed as a node package.
Then `vosk` could be added as a dependency in any `nodejs` project.

Each "models", as in language models, need to be present on the system...
We could probably create a script which would help download all models for language available on the PeerTube instance via Peertube API.

## `vosk-server`

Websocket Server expose `vosk-api` through different protocols: MQTT, GRPC, WebRTC and Websocket

The most obvious to use with Web technology probably is the Websocket protocol. Furthermore it enables 2 way communication, which would come handy to notify the client when a treament is over.

### WebSocket server

#### Cpp

There is Docker image running the cpp version of the Ws server :
https://github.com/alphacep/vosk-server/blob/master/docker/Dockerfile.kaldi-beast-en
This image only load one language package (en-us), this must adapted to load other languges.

This other image run the Pythong version of the Ws server withe the fr language package:
https://github.com/alphacep/vosk-server/blob/master/docker/Dockerfile.kaldi-fr

> Other images variant are avalable there:
> https://github.com/alphacep/vosk-server/blob/master/docker

How does it works? Mostly like the Python server, see below

#### Python

The Python Ws server is pretty basic:
Server:
https://github.com/alphacep/vosk-server/blob/master/websocket/asr_server.py
Test:
https://github.com/alphacep/vosk-server/blob/master/websocket/test.py

It enables to send audio data and retrieve... a json output.

**Example:**

```
docker run -p 2700:2700 alphacep/kaldi-fr:latest
git clone git@github.com:alphacep/vosk-server.git
cd vosk-server/websocket
python3 ./test.py test.wav
```

**Output the following:**

```
{
  "partial" : ""
}
// [...] more partials
// and finally:
{
  "result" : [{
      "conf" : 0.366513,
      "end" : 3.011505,
      "start" : 2.825967,
      "word" : "c'est"
    }, {
      "conf" : 0.225795,
      "end" : 3.408261,
      "start" : 3.330000,
      "word" : "au"
    }, {
      "conf" : 0.338492,
      "end" : 3.930000,
      "start" : 3.408261,
      "word" : "réconfortant"
    }],
  "text" : "c'est au réconfortant"
}
```

> _/!\ The current Ws server implementation doesn't allow language configuration.
> Meaning we'll need one Ws server per language in current implementation;
> not very practical but doable... fro example:_
>
> - fr.vosk.education.fr
> - en.vosk.education.fr
> - ....
>   Plus we'll probabably need another service which will do the conversion to the correct audio format

We'll probably need to code our own web service, which will handle conversion as well as language configuration:

- /set_language
  input: language
  output: boolean

- /
  input: video data
  output: link to a srt file?

## Models

:fr: https://github.com/pguyot/zamia-speech/releases
