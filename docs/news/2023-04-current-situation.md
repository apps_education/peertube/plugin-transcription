# April 2023

It's been 6 months since I worked on the plugin so here is the current situation.

## Our issues

- 🟥 [Transcription process may stop in the middle of a transcription without an error message](https://gitlab.com/apps_education/peertube/plugin-transcription/-/issues/48)
  - This requires further investigation:
    - how to improve ~~verbosity~~ **usefulness** of the logs in such cases?
    - track memory/cpu usages
- 🟥 Sometimes transcription is not triggered on video creation

  - **@chocobozz** is ready to help 💛 and is waiting for a confirmation from our part:
  - https://github.com/Chocobozzz/PeerTube/issues/5221#issuecomment-1289005549
  - https://github.com/Chocobozzz/PeerTube/issues/5343

- 🟧 Expose server push notification to plugin

  - This is still very annoying, the user has no clues on what's going on under the hood. The only way is to get hands dirty and dig into the logs: we need to work with **@chocobozz** on this !

- 🟧 What about **[Vosk](https://alphacephei.com/vosk/)** ? 
  - Maintenance status 
  - War in Ukraine 🇺🇦 
  - Can we maintain [the custom forks](../vosk.md) we made ? 
  - Anyway is **Vosk** still relevant with all the new kids on the block ? 🤖 

> See https://gitlab.com/apps_education/peertube/plugin-transcription/-/issues/45

## Users issues

- 🟧 [Server crashes with big model](https://gitlab.com/apps_education/peertube/plugin-transcription/-/issues/47)

  - This issue was left unresolved and was happening withing a **Docker** based installation. This is most likely a "nos space left on device" issue... but we'll probably play with **Docker** ourselves in months to come ! 🐳

- 🆕 [Certificate expired, cannot install transcription](https://gitlab.com/apps_education/peertube/plugin-transcription/-/issues/49)

- and other older installation issues which may not be relevant anymore...
  - [The plugin does nothing](https://gitlab.com/apps_education/peertube/plugin-transcription/-/issues/43)
  - [Peertube-plugin-transcription on raspberry pi docker install](https://gitlab.com/apps_education/peertube/plugin-transcription/-/issues/42)
  - [Server Crashes on video update after caption deleted.](https://gitlab.com/apps_education/peertube/plugin-transcription/-/issues/44)

## What's new in **PeerTube** 5.x.x?

We need to get up to date with the latest features and see if some could enhance the plugin UX! 🍬
