# WAAS (Whisper as a service)

> https://github.com/schibsted/WAAS

It consists in a backend crafted in __Python__ with __Flask__ and [`rq`](https://python-rq.org/) (__Redis__ queue) to handle job queueing.

It runs `openai-whisper` job to carry out the transcription process.

A frontend (made in [preact](https://preactjs.com/)).
> __⚠ Warning:__ minified version of the libs are commited.

It seems the frontend also supports some kind of caption edition feature using a custom `*.jojo` format.

__Made by:__

> *__schibsted__ a Nordic family of digital brands. [..]*

>*__VG__ (Verdens Gang) is Norway's leading media house with more than 2 million daily users through VG.no and VG's printed newspaper. [...]*

__Contact:__ jojo@vg.no

## Usecase
How it would work with the __PeerTube__ transcription plugin :
```mermaid
sequenceDiagram
    participant PeerTube
    participant plugin
    participant WAAS
    
    PeerTube-)plugin: action:api.video.uploaded
    Note right of PeerTube: A hook is triggered
    plugin->>+WAAS: POST /v1/transcribe
    WAAS-->>plugin: job_id
    WAAS-)WAAS: Transcription
    WAAS-)-plugin: POST /mywebhook
    plugin->>WAAS: GET /v1/download/{job_id}
    Note right of plugin: Download caption
    plugin->>PeerTube: POST /api/v1/videos/1/captions/fr
```

> Current hook `action:api.video.uploaded` isn't reliable see https://github.com/Chocobozzz/PeerTube/issues/5343#issuecomment-1500366936

## Tasks

- setup and run a WAAS on digital ocean (docker, redis n co) and name it

- setup and run a WAAS locally with docker-compose for development purpose (callback URL in http://localhost)

- fork current plugin and rebrand it : peertube-plugin-transcription-whisper
    - warn the followers of the issue
    - mention fork URL in the README
- nearly 0 config at first: WAAS base URL and attempt to post a sample job to the service
    - don't translate by default
- design what's a "job" and handle persistence
    - job_id
    - user_id
    - video_id
    - started_at
    - state: success / failure /unknown
- handle callback return success & failure
- download caption and upload it back to PeerTube
- deploy it and test it on a PeerTube test
