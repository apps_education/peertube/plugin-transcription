> **⚠️ Deprecation notice:** [Since __PeerTube 6.2__](https://joinpeertube.org/news/release-6.2) __transcription__ is now a core __PeerTube__ feature which makes this plugin somewhat redundant.
> A [transcription package](https://github.com/Chocobozzz/PeerTube/tree/release/6.2.0/packages/transcription) is now maintained in the __PeerTube__ codebase.
> This feature is available in either __PeerTube__ or `peertube-runner`.
> It uses __Whisper__ internally, but a __Vosk__ version could also be implemented.

# PeerTube Transcription Plugin

A **PeerTube** plugin generating subtitles for your videos via _Automatic Speech Recognition_.

> Powered by [Vosk](https://alphacephei.com/vosk/).

## Installation

### PeerTube UI

Browse to **Administration > Plugin/Themes > Search** and type _transcription_ in the search bar:
![Searching the PeerTube plugins catalogue][installation]

Click **Install** then **Confirm**.

### Yarn (or npm) script

```bash
yarn plugin:install -n peertube-plugin-transcription
```

> In **production** you may have to specify env variables:
>
> ```bash
> cd /var/www/peertube/peertube-latest/
> NODE_ENV=production NODE_CONFIG_DIR=/var/www/peertube/config yarn plugin:install -n peertube-plugin-transcription
> ```

### PeerTube CLI

Setup **PeerTube** CLI:
[https://docs.joinpeertube.org/maintain-tools?id=installation](https://docs.joinpeertube.org/maintain-tools?id=installation)

```bash
peertube plugins install --npm-name peertube-plugin-transcription
```

## Usage

The video transcription happens when you **Update** a video and set its language.

The process is executed in the background and, in our tests, lasted for half the time of the video.

> For example, it took approximately 5 minutes to transcript a 10 minutes long video.

There are currently no notifications to warn the user when the process is over (see [Chocobozzz/PeerTube#5076](https://github.com/Chocobozzz/PeerTube/issues/5076).
If it succeeded, a caption should be present in the **Captions** tab of the video edition page.

> If not, feel free to [fill an issue](https://gitlab.com/apps_education/peertube/plugin-transcription/-/issues/new).

By default, a _light_ **language training model** is used, so you may be disappointed by the quality of the transcript
and may wish to choose a larger _model_ for better results (see below).

### Settings

From the **transcription** plugin settings page, you may choose a **language training model** for the available languages.

![Transcription plugin configuration page][configuration]

You'll probably need to configure only the few you'll be using on your instance since some of them may weight up to 2 Gb!

> Only languages with available training are currently proposed.
> Checkout the following page for a better description of each available _models_:
> https://alphacephei.com/vosk/models

## Documentation

- [Development][]
  - [Continuous Integration][]
  - [Continuous Deployment][]
- [Vosk][]
- [ffmpeg][]

## About

Initial codebase developed with the financial support of the [_Direction du Numérique pour l'Éducation_ (DNE)](https://www.education.gouv.fr/direction-du-numerique-pour-l-education-dne-9983)
for the ministry of _Éducation nationale, de la Jeunesse et des Sports_ (french ministry of education) in the context of the [apps.education.fr](https://apps.education.fr/) initiative.

## License

[EUPL][]

<!--
   These are the links to docs and images since proposed solutions don't seem to work for Gitlab:
   - https://npm.community/t/relative-links-in-npm-readme-markdown-not-functional/1525.html
   - https://stackoverflow.com/questions/62575382/relative-link-from-readme-md-to-another-file-in-package-rendered-in-npmjs
-->

[development]: https://gitlab.com/apps_education/peertube/plugin-transcription/-/blob/main/docs/development.md
[continuous integration]: https://gitlab.com/apps_education/peertube/plugin-transcription/-/blob/main/docs/development.md#continuous-integration
[continuous deployment]: https://gitlab.com/apps_education/peertube/plugin-transcription/-/blob/main/docs/development.md#continuous-deployment
[vosk]: https://gitlab.com/apps_education/peertube/plugin-transcription/-/blob/main/docs/vosk.md
[ffmpeg]: https://gitlab.com/apps_education/peertube/plugin-transcription/-/blob/main/docs/ffmpeg.md
[EUPL]: https://gitlab.com/apps_education/peertube/plugin-transcription/-/blob/main/LICENSE
[installation]: https://gitlab.com/apps_education/peertube/plugin-transcription/-/raw/main/docs/images/installation.png
[configuration]: https://gitlab.com/apps_education/peertube/plugin-transcription/-/raw/main/docs/images/configuration.png
